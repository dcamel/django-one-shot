#Feature 8: adding get_object_or_404
#Feature 9: adding redirect
from django.shortcuts import render, get_object_or_404, redirect
#Feature 7: adding TodoList
#Feature 13: adding TodoItem
from todos.models import TodoList, TodoItem
#Feature 9: adding this from our created forms.py
#Feature 12: adding TodoForm2 for itemlist editing
from todos.forms import TodoForm, TodoForm2

# Create your views here.

#Feature 7: adding main list for list.html
def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "TodoList": todolists,
    }
    return render(request, "todos/list.html", context)
    #the context part allows variables to be pulled with HTML
    #adding "context" to return line makes it show


#Feature 8: adding details for detail.html
def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        'todo_list_detail': detail,
    }
    return render(request, "todos/detail.html", context)

#Feature 9: adding create_model_name for create.html.  We need to create forms.py first
def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect(
                #"todo_list_list")
                #I am going to put mainpage in first, then try details/id/ after
                "todo_list_detail", id=model_instance.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


#Feature 10: adding todo_list_edit
#I don't know what to call tdl_update .... it didn't matter, call whatever
def todo_list_update(request, id):
    tdl_update = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=tdl_update)
        if form.is_valid():
            form.save()

            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=tdl_update)

    context = {
        "tdl_update": tdl_update,
        "form": form,
    }
    return render(request, "todos/update.html", context)


#Feature11: adding todo_list_delete
def todo_list_delete(request,id):
    deletethis = TodoList.objects.get(id=id)
    if request.method == "POST":
        deletethis.delete()
        return redirect('todo_list_list')

    return render(request, "todos/delete.html")


#Feature 12: adding a todo_item_create
#do we need to change forms?
def todo_item_create(request):
    if request.method == "POST":
        form = TodoForm2(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect(
                #"todo_list_list")
                #I am going to put mainpage in first, then try details/id/ after
                "todo_list_detail", id=model_instance.list.id)
    else:
        form = TodoForm2()
    context = {
        "form": form,
    }
    return render(request, "todos/items/create_item.html", context)


#Feature 13: adding edit feature for TodoItem
def todo_item_update(request, id):
    tditem_update = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoForm2(request.POST, instance=tditem_update)
        if form.is_valid():
            form.save()

            return redirect("todo_list_detail", id=tditem_update.list.id)
    else:
        form = TodoForm2(instance=tditem_update)

    context = {
        "tdl_update": tditem_update,
        "form": form,
    }
    return render(request, "todos/items/update_item.html", context)

#            return redirect("todo_list_detail", id=tditem_update.list.id)
# how the PISS was I supposed to figure that out lmao



##Examples

#For function views, you just use that filter when you're creating your context.

# def my_view(request):
#     # other code here to do other stuff
#     context = {
#         "whatever key name": MyModel.objects.filter(property=value),
#         # other context variables, if needed
#     }
#     return render(request, "whatever/template.html", context)



# List views
# def show_model_name(request):
#   model_list = ModelName.objects.all()
#   context = {
#     "model_list": model_list
#   }
#   return render(request, "model_names/list.html", context)




# Detail views
# def show_model_name(request, id):
#   model_instance = ModelName.objects.get(id=id)
#   context = {
#     "model_instance": model_instance
#   }
#   return render(request, "model_names/detail.html", context)




# Create views
# def create_model_name(request):
#   if request.method == "POST":
#     form = ModelForm(request.POST)
#     if form.is_valid():
#       # To redirect to the detail view of the model, use this:
#       model_instance = form.save()
#       return redirect("detail_url", id=model_instance.id)

#       # To add something to the model, like setting a user,
#       # use something like this:
#       #
#       # model_instance = form.save(commit=False)
#       # model_instance.user = request.user
#       # model_instance.save()
#       # return redirect("detail_url", id=model_instance.id)
#   else:
#     form = ModelForm()

#   context = {
#     "form": form
#   }

#   return render(request, "model_names/create.html", context)




# Update views
# def update_model_name(request, id):
#   model_instance = ModelName.objects.get(id=id)
#   if request.method == "POST":
#     form = ModelForm(request.POST, instance=model_instance)
#     if form.is_valid():
#       # To redirect to the detail view of the model, use this:
#       model_instance = form.save()
#       return redirect("detail_url", id=model_instance.id)

#       # To add something to the model, like setting a user,
#       # use something like this:
#       #
#       # model_instance = form.save(commit=False)
#       # model_instance.user = request.user
#       # model_instance.save()
#       # return redirect("detail_url", id=model_instance.id)
#   else:
#     form = ModelForm(instance=model_instance)

#   context = {
#     "form": form
#   }

#   return render(request, "model_names/edit.html", context)




# Delete views
# def delete_model_name(request, id):
#   model_instance = ModelName.objects.get(id=id)
#   if request.method == "POST":
#     model_instance.delete()
#     return redirect("some_url")

#   return render(request, "models_names/delete.html")
