#Feature 9:  created and entering all this in for the Create Todo list portion
#Feature 12: adding TodoItem
from django.forms import ModelForm
from todos.models import TodoList, TodoItem

#Feature 9: original Form for creating new Todo Lists
class TodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
            #I think we just use name for now.  Date is automatic.  Yea we only needed 'name'.
        ]

#Feature 12: adding TodoItem
class TodoForm2(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]


#Notes from Scrumptious project
# We define a Django model form class by writing our own class that inherits from the django.forms.ModelForm class.
# Then, we create what's called an "inner class" named Meta, which is just a class inside a class.
# Then, we specify which Django model it should work with.
# Finally, we specify which fields we want to show.
