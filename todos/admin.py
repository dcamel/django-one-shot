from django.contrib import admin

from todos.models import TodoList, TodoItem
# Feature 4: Added our first model, TodoList
# Feature 6: Added 2nd model, TodoItem


# Register your models here.


#Feature4: Added our first model, TodoList
@admin.register(TodoList)
class TodoAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]

#Feature6: Added 2nd model, TodoItem
@admin.register(TodoItem)
class TodoAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
    ]









# Example from Scrumptious
# @admin.register(Recipe)
# class RecipeAdmin(admin.ModelAdmin):
#     list_display = [
#         "title",
#         "id",
#     ]
