#Feature 7: Added this file while doing the views portion
#Feature 8: Adding todo_list_detail
#Feature 9: Adding todo_list_create
#Feature 10: Adding todo_list_update
#Feature 11: Adding todo_list_delete
#Feature 12: Adding todo_item_create
#Feature 13: Adding todo_item_update
from django.urls import path
from todos.views import todo_list_list, todo_list_detail, todo_list_create, todo_list_update, todo_list_delete, todo_item_create, todo_item_update


urlpatterns = [
    #Feature 7: adding first views
    path("", todo_list_list, name="todo_list_list"),

    #Feature 7:  Note this link does not work, was in old notes vvvvv
    #path("todos/", todo_list_list, name="todo_list_list"),

    #Feature 8: adding the URL
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),

    #Feature 9: adding createform URL
    path("create/", todo_list_create, name="todo_list_create"),

    #Feature 10: adding edit/update URL for main Todo Lists
    #F10 instructions want a shitty url for update.  Here's the good shit below VVV.  Sorry you read this, Chris and Riley.
    #path("update/<int:id>/", todo_list_update, name="todo_list_update"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),

    #Feature 11: adding delete URL
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),

    #Feature 12: adding TodoItem URL
    path("items/create/", todo_item_create, name="todo_item_create"),

    #Feature 13: adding edit/update URL for items
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
]
