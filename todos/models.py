from django.db import models

# Create your models here.


#Feature 3: added TodoList
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

#Feature 6: extra I'm doing
#this wasn't needed at start, but helps after foreignkeys
#Allows to be able to see name of List when entering Items in admin panel
    def __str__(self):
        return self.name



#Feature 5:  added TodoItem
class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(
        null=True,
        blank=True,
    )
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        TodoList,
        related_name="items",
        on_delete=models.CASCADE,
    )



#Example from learn2:
# class MyModel(models.Model):
#     #This defines the fields of your model
#     name = models.CharField(max_length=100)

#     # This tells Django how to convert our model into a string
#     # when we print() it, or when the admin displays it.
#     def __str__(self):
#         return self.name
